<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

class ExportExcel implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    private $data;

    function __construct($data)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return collect($this->data);
    }
}
