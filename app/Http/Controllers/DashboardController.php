<?php

namespace App\Http\Controllers;

use App\Exports\ExportExcel;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Http;
use Maatwebsite\Excel\Facades\Excel;

class DashboardController extends BaseController
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (session()->get('login')  == null || !session()->get('login')) {
                Redirect::to('/')->send();
            }

            return $next($request);
        });
    }

    public function dashboard()
    {

        $date = new DateTime('last day of this month');
        $lastDay = $date->format('d');

        $getAllData = Http::asForm()->post(env('URL_DATA'), [
            'request' => "get-all"
        ]);

        $getClosedNumber = Http::asForm()->post(env('URL_MENTAH'), [
            'request' => "get-data-close-number-today"
        ])->json()['data'];

        $getLainnya = Http::asForm()->post(env('URL_MENTAH'), [
            'request' => "get-lainnya"
        ])->json()['data'];

        $result = $getAllData->json()['data'];

        // Store All Data
        $sumChangeCard = 0;
        $sumNewActivation = 0;
        $sumPendingActivation = 0;
        $sumCsat = 0;
        $sumClosedNumber = count($getClosedNumber);
        $sumLainnya = count($getLainnya);
        $chart = [
            'change_card' => [],
            'new_activation' => [],
            'pending_activation' => [],
            'csat' => [],
            'closed_number' => [],
            'lainnya' => [],
        ];

        for ($i = 1; $i <= $lastDay; $i++) {
            $chart['change_card'][$i] = 0;
            $chart['new_activation'][$i] = 0;
            $chart['pending_activation'][$i] = 0;
            $chart['csat'][$i] = 0;
            $chart['closed_number'][$i] = 0;
            $chart['lainnya'][$i] = 0;
        }
        foreach ($getClosedNumber as $closedNumber) {
            $getDate = (int) explode("/", $closedNumber[0])[0];
            $chart['closed_number'][$getDate]++;
        }

        foreach ($getLainnya as $lainnya) {
            $getDate = (int) explode("/", $lainnya[0])[0];
            $chart['lainnya'][$getDate]++;
        }

        foreach ($result as $r) {
            switch ($r[6]) {
                case "Change Card":
                    $getDate = (int) explode("/", $r[0])[0];
                    $chart['change_card'][$getDate]++;
                    $sumChangeCard++;
                    break;
                case "New Activation":
                    $getDate = (int) explode("/", $r[0])[0];
                    $chart['new_activation'][$getDate]++;
                    $sumNewActivation++;
                    break;
                case "Pending Activation":
                    $getDate = (int) explode("/", $r[0])[0];
                    $chart['pending_activation'][$getDate]++;
                    $sumPendingActivation++;
                    break;
                case "CSAT":
                    $getDate = (int) explode("/", $r[0])[0];
                    $chart['csat'][$getDate]++;
                    $sumCsat++;
                    break;
                default:
                    break;
            }
        }

        return view('dashboard', ['sumChangeCard' => $sumChangeCard, 'sumNewActivation' => $sumNewActivation, 'sumPendingActivation' => $sumPendingActivation, 'sumCsat' => $sumCsat, 'sumClosedNumber' => $sumClosedNumber, 'sumLainnya' => $sumLainnya, 'charts' => $chart]);
    }

    public function change_card()
    {
        $getAllData = Http::asForm()->post(env('URL_DATA'), [
            'request' => "get-changed-card"
        ])->json()['data'];

        return view('change_card', ['datas' => $getAllData]);
    }

    public function new_activations()
    {
        $getAllData = Http::asForm()->post(env('URL_DATA'), [
            'request' => "get-new-activation"
        ])->json()['data'];

        return view('new_activations', ['datas' => $getAllData]);
    }

    public function pending_activations()
    {
        $getAllData = Http::asForm()->post(env('URL_DATA'), [
            'request' => "get-pending-activation"
        ])->json()['data'];

        return view('pending_activations', ['datas' => $getAllData]);
    }

    public function csat()
    {
        $getAllData = Http::asForm()->post(env('URL_DATA'), [
            'request' => "get-csat"
        ])->json()['data'];

        return view('csat', ['datas' => $getAllData]);
    }

    public function closed_number()
    {
        $getAllData = Http::asForm()->post(env('URL_MENTAH'), [
            'request' => "get-data-close-number-today"
        ])->json()['data'];

        return view('closed_number', ['datas' => $getAllData]);
    }

    public function end_contract()
    {
        $getAllData = Http::asForm()->post(env('URL_MENTAH_END_CONTRACT'), [
            'request' => "get-finished"
        ])->json()['data'];

        return view('end_contract', ['datas' => $getAllData]);
    }

    public function lainnya()
    {
        $getAllData = Http::asForm()->post(env('URL_MENTAH'), [
            'request' => "get-lainnya"
        ])->json()['data'];

        return view('lainnya', ['datas' => $getAllData]);
    }

    public function export_change_card()
    {
        $getAllData = Http::asForm()->post(env('URL_DATA'), [
            'request' => "get-changed-card"
        ])->json()['data'];

        $header = ['Tanggal Visit', 'Gerai', 'Nama Customer', 'MSISDN', 'MSISDN Lainnya', 'Paket', 'Kategori', 'Start Contract', 'End Contract', 'NIK Sales', 'Score', 'Notes', 'After Call', 'Bukti'];

        array_unshift($getAllData, $header);

        return Excel::download(new ExportExcel($getAllData), 'change_card.xlsx');
    }

    public function export_new_activations()
    {
        $getAllData = Http::asForm()->post(env('URL_DATA'), [
            'request' => "get-new-activation"
        ])->json()['data'];

        $header = ['Tanggal Visit', 'Gerai', 'Nama Customer', 'MSISDN', 'MSISDN Lainnya', 'Paket', 'Kategori', 'Start Contract', 'End Contract', 'NIK Sales', 'Score', 'Notes', 'After Call', 'Bukti'];

        array_unshift($getAllData, $header);

        return Excel::download(new ExportExcel($getAllData), 'new_activations.xlsx');
    }

    public function export_pending_activations()
    {
        $getAllData = Http::asForm()->post(env('URL_DATA'), [
            'request' => "get-pending-activation"
        ])->json()['data'];

        $header = ['Tanggal Visit', 'Gerai', 'Nama Customer', 'MSISDN', 'MSISDN Lainnya', 'Paket', 'Kategori', 'Start Contract', 'End Contract', 'NIK Sales', 'Score', 'Notes', 'After Call', 'Bukti'];

        array_unshift($getAllData, $header);

        return Excel::download(new ExportExcel($getAllData), 'pending_activations.xlsx');
    }

    public function export_csat()
    {
        $getAllData = Http::asForm()->post(env('URL_DATA'), [
            'request' => "get-csat"
        ])->json()['data'];

        $header = ['Tanggal Visit', 'Gerai', 'Nama Customer', 'MSISDN', 'MSISDN Lainnya', 'Paket', 'Kategori', 'Start Contract', 'End Contract', 'NIK Sales', 'Score', 'Notes', 'After Call', 'Bukti'];

        array_unshift($getAllData, $header);

        return Excel::download(new ExportExcel($getAllData), 'csat.xlsx');
    }

    public function export_closed_number()
    {
        $getAllData = Http::asForm()->post(env('URL_MENTAH'), [
            'request' => "get-data-close-number-today"
        ])->json()['data'];

        $header = ['Timestamp', 'Tanggal Visit', 'Gerai', 'Nama Customer', 'MSISDN', 'Paket', 'Kategori', 'Start Contract', 'End Contract', 'NIK Sales', 'Score', 'Notes', 'Bukti Screenshot', 'MSISDN Lain', 'Status'];

        array_unshift($getAllData, $header);

        return Excel::download(new ExportExcel($getAllData), 'closed_number.xlsx');
    }

    public function export_end_contract()
    {
        $getAllData = Http::asForm()->post(env('URL_MENTAH_END_CONTRACT'), [
            'request' => "get-finished"
        ])->json()['data'];

        $header = ['msisdn', 'name_customer', 'end_date', 'account_status', 'package_name', 'first_offer_1', 'first_offer_2', 'last_offer', 'store', 'many_calls', 'result_call_1', 'result_call_2', 'result_call_3'];

        array_unshift($getAllData, $header);

        return Excel::download(new ExportExcel($getAllData), 'end_contract.xlsx');
    }

    public function export_lainnya()
    {
        $getAllData = Http::asForm()->post(env('URL_MENTAH'), [
            'request' => "get-lainnya"
        ])->json()['data'];

        $header = ['Timestamp', 'Tanggal Visit', 'Gerai', 'Nama Customer', 'MSISDN', 'Paket', 'Kategori', 'Start Contract', 'End Contract', 'NIK Sales', 'Score', 'Notes', 'Bukti Screenshot', 'MSISDN Lain', 'Status'];

        array_unshift($getAllData, $header);

        return Excel::download(new ExportExcel($getAllData), 'data_lainnya.xlsx');
    }

    public function changepass()
    {
        return view('changepass');
    }

    public function aksichangepass(Request $request)
    {

        if (md5($request->oldpass) != $request->session()->get('password')) {
            return redirect()->back()->with('error', 'Password tidak sama dengan Password Lama');
        } else {
            Http::asForm()->post(env('URL_LOGIN'), [
                'nik' => $request->session()->get('nik'),
                'password' => $request->newpass,
                'request' => "change-password"
            ]);
            Session::put('password', $request->newpass);
            return redirect('/dashboard')->with('msg', 'Password berhasil diubah !!');
        }
    }

    public function logout()
    {
        Session::flush();
        return redirect('/')->with('error', 'User Logout !!');
    }
}
