<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function login()
    {
        return view('login');
    }

    public function aksilogin(Request $request)
    {
        $get = Http::asForm()->post(env('URL_LOGIN'), [
            'nik' => $request->nik,
            'password' => $request->password,
            'request' => "login"
        ]);

        $result = $get->json();

        if ($result["result"]) {
            session(['login' => true]);
            Session::put('nik', $result["data"][0]);
            Session::put('nama', $result["data"][1]);
            Session::put('password', $result["data"][2]);

            return redirect("/dashboard");
        } else {
            if ($result["status"] == "nik/pass wrong") {
                return redirect()->back()->with('error', 'Wrong Username/Password');
            } else if ($result["status"] == "missing nik/pass") {
                return redirect()->back()->with('error', 'Missing Username/Password');
            }
        }
    }
}
