@extends('main')

@Section('title', 'End Contract')

@section('content')
<div class="page-body dashboard-2-main">

  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <!-- Server Side Processing start-->
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            @if(count($datas) > 0)
            <div class="row">
              <div class="col">
                <h5 class="float-left">End Contract</h5>
              </div>
              <div class="col-2 float-right">
                <a href="/export-lihat-end-contract" class="btn btn-primary">Export Data</a>
              </div>
            </div>
            @else
            <h5 class="float-left">End Contract</h5>
            @endif
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="display datatables" id="end_contract_table">
                <thead>
                  <tr>
                    <th>Msisdn</th>
                    <th>Nama</th>
                    <th>Akhir Paket</th>
                    <th>Store</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @if(count($datas) > 0)
                  @foreach($datas as $data)
                  <tr>
                    <td>{{$data[0]}}</td>
                    <td>{{$data[1]}}</td>
                    <td>{{$data[2]}}</td>
                    <td>{{$data[8]}}</td>
                    <td>
                      <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#notesModal{{$data[3]}}">
                        Lihat Detail
                      </button>
                    </td>
                  </tr>
                  @endforeach
                  @else
                  <tr>
                    <td colspan="9" class="text-center bg-dark">Tidak Ada Data</td>
                  </tr>
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- Server Side Processing end-->
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>
@foreach($datas as $data)
<div class="modal fade" id="notesModal{{$data[3]}}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Detail</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col">
            Nama Paket: {{$data[4]}} <br>
            Offer 1: {{$data[5]}} <br>
            Offer 2: {{$data[6]}} <br>
            Offer 3: {{$data[7]}} <br>
          </div>
          <div class="col">
            <?php
            function resultCall($result)
            {
              $html = '';
              switch ($result) {
                case "NOT PICKING UP":
                  $html = '<span class="badge badge-info">Tidak Diangkat</span>';
                  break;
                case "ACCEPTED":
                  $html = '<span class="badge badge-success">Diterima</span>';
                  break;
                case "REJECTED":
                  $html = '<span class="badge badge-danger">Ditolak</span>';
                  break;
              }

              return $html;
            }
            ?>
            Hasil Panggilan 1: {!!resultCall($data[10])!!} <br>
            Hasil Panggilan 2: {!!resultCall($data[11])!!} <br>
            Hasil Panggilan 3: {!!resultCall($data[12])!!}
            <br>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        @if(isset($data[13]))
        <a href="{{$data[13]}}" class="btn btn-primary" target="_blank">Lihat Bukti</a>
        @endif
      </div>
    </div>
  </div>
</div>
@endforeach
@endsection

@section('custom-js')
<script>
  $(document).ready(function() {
    $('#end_contract_table').DataTable();
  });
</script>
@endsection