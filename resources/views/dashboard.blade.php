@extends('main')

@Section('title', 'Dashboard')

@section('content')
<div class="page-body dashboard-2-main">
    @if (session('msg'))
    <div class="alert alert-success" role="alert">
        {{ session('msg') }}
    </div>
    @endif
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">

            {{-- Category Section --}}
            <div class="col-xl-12">
                <div class="row">
                    <div class="col chart_data_right">
                        <div class="card income-card card-secondary">
                            <div class="card-body align-items-center">
                                <div class="round-progress knob-block text-center">
                                    <h5>{{$sumChangeCard}}</h5>
                                    <p>Change Card</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col chart_data_right">
                        <div class="card income-card card-secondary">
                            <div class="card-body align-items-center">
                                <div class="round-progress knob-block text-center">
                                    <h5>{{$sumNewActivation}}</h5>
                                    <p>New Activations</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col chart_data_right">
                        <div class="card income-card card-secondary">
                            <div class="card-body align-items-center">
                                <div class="round-progress knob-block text-center">
                                    <h5>{{$sumPendingActivation}}</h5>
                                    <p>Pending Activations</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col chart_data_right">
                        <div class="card income-card card-secondary">
                            <div class="card-body align-items-center">
                                <div class="round-progress knob-block text-center">
                                    <h5>{{$sumCsat}}</h5>
                                    <p>CSAT</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">


                    <div class="col chart_data_right">
                        <div class="card income-card card-secondary">
                            <div class="card-body align-items-center">
                                <div class="round-progress knob-block text-center">
                                    <h5>{{$sumClosedNumber}}</h5>
                                    <p>Closed Number</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col chart_data_right">
                        <div class="card income-card card-secondary">
                            <div class="card-body align-items-center">
                                <div class="round-progress knob-block text-center">
                                    <h5>{{$sumLainnya}}</h5>
                                    <p>Lainnya</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Chart Sections --}}
            <div class="col-xl-12 box-col-12 des-xl-100 invoice-sec">
                <div class="card">
                    <div class="card-header">
                        <div class="header-top d-sm-flex justify-content-between align-items-center">
                            <h5>Kunjungan Untuk Bulan ini</h5>
                            <div class="setting-list">
                                <ul class="list-unstyled setting-option">
                                    <li>
                                        <div class="setting-primary"><i class="icon-settings"></i></div>
                                    </li>
                                    <li><i class="icofont icofont-maximize full-card font-primary"></i>
                                    </li>
                                    <li><i class="icofont icofont-minus minimize-card font-primary"></i>
                                    </li>
                                    </li>
                                    <li><i class="icofont icofont-error close-card font-primary"> </i></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <div id="timeline-chart"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>
@endsection

@section('custom-js')
<script>
    var options25 = {
        chart: {
            type: "line",
            height: 450,
            foreColor: "#999",
            stacked: true,
            dropShadow: {
                enabled: true,
                enabledSeries: [0],
                top: -2,
                left: 2,
                blur: 5,
                opacity: 0.06,
            },
            toolbar: {
                show: false,
            },
        },
        responsive: [{
                breakpoint: 1470,
                options: {
                    chart: {
                        height: 440,
                    },
                },
            },
            {
                breakpoint: 1365,
                options: {
                    chart: {
                        height: 300,
                    },
                },
            },
            {
                breakpoint: 991,
                options: {
                    chart: {
                        height: 250,
                    },
                },
            },
        ],
        colors: ['#2E93fA', '#66DA26', '#546E7A', '#E91E63', '#FF9800', '#000000'],
        stroke: {
            width: 3,
        },
        dataLabels: {
            enabled: true,
        },
        labels: [
            <?php
            foreach ($charts['change_card'] as $key => $chart) {
                echo $key . ", ";
            }
            ?>
        ],
        series: [{
                name: "Change Card",
                data: generateValue(0, 30),
            },
            {
                name: "New Activation",
                data: generateValue(1, 30),
            },
            {
                name: "Pending Activation",
                data: generateValue(2, 30),
            },
            {
                name: "CSAT",
                data: generateValue(3, 30),
            },
            {
                name: "Closed Number",
                data: generateValue(4, 30),
            },
            {
                name: "Lainnya",
                data: generateValue(5, 30),
            },
        ],
        markers: {
            size: 5,
            strokeColor: "#e3e3e3",
            strokeWidth: 3,
            strokeOpacity: 1,
            fillOpacity: 1,
            hover: {
                size: 6,
            },
        },
        xaxis: {
            type: "number",
            axisBorder: {
                show: false,
            },
            axisTicks: {
                show: false,
            },
            labels: {
                formatter: function(value) {
                    const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
                        "Juli", "Agustus", "September", "Oktober", "November", "Desember"
                    ];

                    const d = new Date();
                    return `${value} ${monthNames[d.getMonth()]} ${new Date().getFullYear()}`;
                },
            },
        },
        yaxis: {
            labels: {
                offsetX: 14,
                offsetY: -5,
            },
            tooltip: {
                enabled: true,
            },
            labels: {
                formatter: function(value) {
                    return parseInt(value) + " Kunjungan";
                },
            },
        },
        grid: {
            padding: {
                left: -5,
                right: 5,
            },
        },
        tooltip: {
            x: {
                format: "dd MMM yyyy",
            },
        },
        legend: {
            position: "top",
            horizontalAlign: "left",
            show: false,
        },
        fill: {
            type: "solid",
            fillOpacity: 0.7,
        },
    };
    var chart25 = new ApexCharts(
        document.querySelector("#timeline-chart"),
        options25
    );
    chart25.render();

    function generateValue(s, count) {
        var values = [
            [
                <?php
                foreach ($charts['change_card'] as $key => $chart) {
                    echo $chart . ", ";
                }
                ?>
            ],
            [
                <?php
                foreach ($charts['new_activation'] as $key => $chart) {
                    echo $chart . ", ";
                }
                ?>
            ],
            [
                <?php
                foreach ($charts['pending_activation'] as $key => $chart) {
                    echo $chart . ", ";
                }
                ?>
            ],
            [
                <?php
                foreach ($charts['csat'] as $key => $chart) {
                    echo $chart . ", ";
                }
                ?>
            ],
            [
                <?php
                foreach ($charts['closed_number'] as $key => $chart) {
                    echo $chart . ", ";
                }
                ?>
            ],
            [
                <?php
                foreach ($charts['lainnya'] as $key => $chart) {
                    echo $chart . ", ";
                }
                ?>
            ],
        ];
        return values[s];
        // return series;
    }

    function getDate() {
        let date = new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).getDate();
        let arr = [];

        for (let i = 1; i <= date; i++) {
            arr.push(i);
        }
        return arr;
    }
</script>
@endsection