@extends('main')

@Section('title', 'Kategori Lainnya')

@section('content')
<div class="page-body dashboard-2-main">

  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <!-- Server Side Processing start-->
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            @if(count($datas) > 0)
            <div class="row">
              <div class="col">
                <h5 class="float-left">Kategori Lainnya</h5>
              </div>
              <div class="col-2 float-right">
                <a href="/export-lihat-lainnya" class="btn btn-primary">Export Data</a>
              </div>
            </div>
            @else
            <h5 class="float-left">Kategori Lainnya</h5>
            @endif
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="display datatables" id="lainnya_table">
                <thead>
                  <tr>
                    <th>Tanggal Visit</th>
                    <th>Gerai</th>
                    <th>Nama</th>
                    <th>Msisdn</th>
                    <th>Paket</th>
                    <th>Kontrak</th>
                    <th>Sales</th>
                    <th>Kategori</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @if(count($datas) > 0)
                  @foreach($datas as $data)
                  <tr>
                    <td>{{$data[1]}}</td>
                    <td>{{$data[2]}}</td>
                    <td>{{$data[3]}}</td>
                    <td>{{$data[4]}} / {{$data[13]}}</td>
                    <td>{{$data[5]}}</td>
                    <td>{{$data[7]}} - {{$data[8]}}</td>
                    <td>{{$data[9]}}</td>
                    <td>{{$data[6]}}</td>
                    <td>
                      <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#notesModal{{$data[4]}}">
                        Notes
                      </button>
                    </td>
                  </tr>
                  @endforeach
                  @else
                  <tr>
                    <td colspan="9" class="text-center bg-dark">Tidak Ada Data</td>
                  </tr>
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- Server Side Processing end-->
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>
@foreach($datas as $data)
<div class="modal fade" id="notesModal{{$data[4]}}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Notes</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        @if($data[11] != '')
        {{$data[11]}}
        @else
        <p class="text-center">Tidak ada Notes</p>
        @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        @if(isset($data[13]))
        <a href="{{$data[13]}}" class="btn btn-primary" target="_blank">Lihat Bukti</a>
        @endif
      </div>
    </div>
  </div>
</div>
@endforeach
@endsection

@section('custom-js')
<script>
  $(document).ready(function() {
    $('#lainnya_table').DataTable();
  });
</script>
@endsection