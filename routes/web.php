<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('login');
// });

Route::get('/', 'App\Http\Controllers\Controller@login');
Route::post('/Aksilogin', 'App\Http\Controllers\Controller@aksilogin');


Route::get('/lihat-change-card', 'App\Http\Controllers\DashboardController@change_card');
Route::get('/lihat-new-activation', 'App\Http\Controllers\DashboardController@new_activations');
Route::get('/lihat-pending-activation', 'App\Http\Controllers\DashboardController@pending_activations');
Route::get('/lihat-csat', 'App\Http\Controllers\DashboardController@csat');
Route::get('/lihat-closed-number', 'App\Http\Controllers\DashboardController@closed_number');
Route::get('/lihat-end-contract', 'App\Http\Controllers\DashboardController@end_contract');
Route::get('/lihat-lainnya', 'App\Http\Controllers\DashboardController@lainnya');
Route::get('/dashboard', 'App\Http\Controllers\DashboardController@dashboard');

Route::get('/export-lihat-change-card', 'App\Http\Controllers\DashboardController@export_change_card');
Route::get('/export-lihat-new-activation', 'App\Http\Controllers\DashboardController@export_new_activations');
Route::get('/export-lihat-pending-activation', 'App\Http\Controllers\DashboardController@export_pending_activations');
Route::get('/export-lihat-csat', 'App\Http\Controllers\DashboardController@export_csat');
Route::get('/export-lihat-closed-number', 'App\Http\Controllers\DashboardController@export_closed_number');
Route::get('/export-lihat-end-contract', 'App\Http\Controllers\DashboardController@export_end_contract');
Route::get('/export-lihat-lainnya', 'App\Http\Controllers\DashboardController@export_lainnya');

Route::get('/ubah-password', 'App\Http\Controllers\DashboardController@changepass');
Route::post('/Aksichangepass', 'App\Http\Controllers\DashboardController@aksichangepass');

Route::get('/logout', 'App\Http\Controllers\DashboardController@logout');
