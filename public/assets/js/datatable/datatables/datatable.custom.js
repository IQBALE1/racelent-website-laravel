$(document).ready(function() {
    
    $('#category1').DataTable({
        "serverSide": true,
        "ajax": "../assets/json/server-side.json"
    } );
    
    $('#category2').DataTable({
        "serverSide": true,
        "ajax": "../assets/json/server-side.json"
    } );
    
    $('#category3').DataTable({
        "serverSide": true,
        "ajax": "../assets/json/server-side.json"
    } );
    
    $('#category4').DataTable({
        "serverSide": true,
        "ajax": "../assets/json/server-side.json"
    } );

    // get data with post link
    // $('#data-source-5').DataTable({
    //     "processing": true,
    //     "serverSide": true,
    //     "ajax": {
    //         "url": "../assets/json/server-side.json",
    //         "data": {
    //             'nik' : '123',
    //             'password' : '123',
    //             'request' : '123'
    //         },
    //         "type": "POST"
    //     }
    // });

    // $.ajax({
    //     url: "https://script.google.com/macros/s/AKfycbz9sFC7RyMeLoEehhAHdIZ5OuhuPhNp1VSpiL7Q0XI8FRDYbRdVvF8Azma8mSxgV9AC/exec",
    //     dataType: "json",
    //     data: {
    //         'nik' : '123',
    //         'password' : '123',
    //         'request' : '123'
    //     },
    //     success: function(data) {
    //         console.log(data);
    //     }
    // });
});